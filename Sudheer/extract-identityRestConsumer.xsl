<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common" 
     xmlns:dp="http://www.datapower.com/extensions"
     xmlns:dpconfig="http://www.datapower.com/param/config" 
    exclude-result-prefixes="dp exsl"
    extension-element-prefixes="dp dpconfig">   
    <xsl:include href="local:/DiskStore/Common/XSLT/setHTTPStatusCode.xsl"/> 
  
    <xsl:template match="/">
    
        <xsl:message dp:priority="debug">**Executing extract-identityRestConsumer.xsl**</xsl:message>
        
         <xsl:variable name="application-context">
			<xsl:choose>
				<xsl:when test="dp:variable('var://context/Saved/applicationContext')">
					<xsl:copy-of select="dp:parse(dp:variable('var://context/Saved/applicationContext'))"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="''"></xsl:value-of>
				</xsl:otherwise>
			</xsl:choose>		
		</xsl:variable>
        <!-- check for authorization header -->
        <xsl:variable name="checkAuthHeader">
        <xsl:choose>
        	<xsl:when test="dp:http-request-header('Authorization')">
        		<!-- <xsl:message dp:priority="error"> auth header found</xsl:message> -->
        		<xsl:value-of select="'true'"></xsl:value-of>
        	</xsl:when>
        	<xsl:otherwise>
        		<xsl:value-of select="'false'"></xsl:value-of>
        	</xsl:otherwise>
        </xsl:choose>
        </xsl:variable>
        <xsl:variable name="uri" select="dp:variable('var://service/URI')"/>
        <xsl:variable name="resource">
			<xsl:choose>
				<xsl:when test="contains($uri,'?')">
			 		<xsl:value-of select="substring-before($uri,'?')"/>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<xsl:value-of select="$uri"/>	
			 	</xsl:otherwise>
			 </xsl:choose>
		</xsl:variable>
		<xsl:variable name="PASCheck" select="dp:http-request-header('application')"/>
		<xsl:variable name="application-name">
			<xsl:choose>
				<xsl:when test="$PASCheck='PAS' and  contains($resource,'/pas/v1/telematics/vehicles/')">
					<xsl:value-of select="$PASCheck"></xsl:value-of>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$application-context/applicationContext/application/text()"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="env" select="dp:variable('var://service/domain-name')"/>
		
		<!-- get output creds -->
		<xsl:variable name="AAAConfig" select="document('local:///DiskStore/APIGateway/config/AAAInfo.xml')"/>
		<xsl:variable name="outputCred" select="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authenticate/Application[Name=$application-name]/OutputCredential/text()"/>

		
		<xsl:choose>
			<xsl:when test="string-length($outputCred) &lt; 1">
				<xsl:call-template name="setErrorObj"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- get authMehotd  [InputCredential=$outputCred]/[InputResource=$resource]/InputResource/@authMethod/text() --> 
				<xsl:variable name="authMethod" select="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authorize[InputCredential/text()=$outputCred]/InputResource[.=$resource]/@authMethod"/>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:choose>	
				<xsl:when test="$PASCheck='PAS' and  contains($resource,'/pas/v1/telematics/vehicles/')">
					<oAuthFlag>false</oAuthFlag>
        			<application><xsl:value-of select="dp:http-request-header('application')"/></application>
        			<clientId><xsl:value-of select="dp:http-request-header('X-Client-Id')"/></clientId>
				</xsl:when>			
				<xsl:when test="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authorize[InputCredential/text()=$outputCred]/InputResource[.=$resource]/@authMethod='oAuth' and $checkAuthHeader='true'">
					<oAuthFlag>true</oAuthFlag>
       				<application><xsl:value-of select="$application-name"/></application>
       				<authToken><xsl:value-of select="substring-after(dp:http-request-header('Authorization'),'Bearer ')"/></authToken>
				</xsl:when>
				<xsl:when test="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authorize[InputCredential/text()=$outputCred]/InputResource[.=$resource]/@authMethod='clientId'">
					<oAuthFlag>false</oAuthFlag>
        			<application><xsl:value-of select="$application-name"/></application>
        			<clientId><xsl:value-of select="dp:http-request-header('X-Client-Id')"/></clientId>
				</xsl:when>
				<xsl:otherwise><dp:reject>No Authentication method set.</dp:reject></xsl:otherwise>
			</xsl:choose>
        <xsl:message dp:priority="debug">application-name:<xsl:value-of select="$application-name"/></xsl:message>
       <!-- 	<xsl:choose>
       		<xsl:when test="$checkAuthHeader='true'">
       			<oAuthFlag>true</oAuthFlag>
       			<application><xsl:value-of select="$application-name"/></application>
       			<authToken><xsl:value-of select="substring-after(dp:http-request-header('Authorization'),'Bearer ')"/></authToken>
       		</xsl:when>
       		<xsl:otherwise>
       			<oAuthFlag>false</oAuthFlag>
        		<application><xsl:value-of select="$application-name"/></application>
        		<clientId><xsl:value-of select="dp:http-request-header('X-Client-Id')"/></clientId>
        	</xsl:otherwise>
        </xsl:choose> -->
    </xsl:template>
      <xsl:template name="setErrorObj">
    	<dp:set-variable name="'var://context/error/errorObjS'" value="'{ &quot;httpErrCd&quot;:&quot;401&quot;, &quot;errorSource&quot;:&quot;AAA extract-identityRestConsumer xslt&quot;, &quot;httpErrMsg&quot;:&quot;Unauthorized&quot;, &quot;moreInfo&quot;:&quot;Authentication method not set&quot; }'"/>
   	<xsl:call-template name="unauthorized"/>
   	</xsl:template>
</xsl:stylesheet>