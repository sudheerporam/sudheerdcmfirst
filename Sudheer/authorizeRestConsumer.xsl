<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common" 
     xmlns:dp="http://www.datapower.com/extensions"
     xmlns:dpconfig="http://www.datapower.com/param/config" 
    exclude-result-prefixes="dp exsl"
    extension-element-prefixes="dp dpconfig">    
    <xsl:include href="local:/DiskStore/Common/XSLT/setHTTPStatusCode.xsl"/>	
    <xsl:template match="/">
        <xsl:message dp:priority="debug">**authorizeRestConsumer.xsl**</xsl:message>
   
   			<xsl:variable name="AAAConfig" select="document('local:///DiskStore/APIGateway/config/AAAInfo.xml')"/>	
   			<xsl:variable name="env" select="dp:variable('var://service/domain-name')"/>	
 			<xsl:variable name="OutputCredential" select="/container/mapped-credentials/entry/authenticated/OutputCredential/text()"></xsl:variable> 
			<xsl:variable name="Resource">
			<xsl:choose>
				<xsl:when test="contains(/container/mapped-resource/resource/item/text(),'?')">
			 		<xsl:value-of select="substring-before(/container/mapped-resource/resource/item/text(),'?')"/>
			 	</xsl:when>
			 	<xsl:otherwise>
			 		<xsl:value-of select="/container/mapped-resource/resource/item/text()"/>	
			 	</xsl:otherwise>
			 </xsl:choose>
			 </xsl:variable>
      
       <!-- Check if the consumer identity has access to requested resource -->
        		<xsl:choose>
		        	<xsl:when test="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authorize[InputCredential/text()=normalize-space($OutputCredential) and InputResource/text()=$Resource]/access/text()='allow'">
		        			<approved></approved>
		        	</xsl:when>
		        	<xsl:otherwise>

						<xsl:call-template name="setErrorObj"/>
		        			<xsl:message dp:type="custom-aaa-audit" dp:priority="info"><xsl:value-of select="'Transaction Rejected as the Authorization Failed !!'"/></xsl:message>
		        	</xsl:otherwise>
        	</xsl:choose>
   </xsl:template>
    <xsl:template name="setErrorObj">
    	<dp:set-variable name="'var://context/error/errorObjS'" value="'{ &quot;httpErrCd&quot;:&quot;401&quot;, &quot;errorSource&quot;:&quot;AAA authorization xslt&quot;, &quot;httpErrMsg&quot;:&quot;Unauthorized&quot;, &quot;moreInfo&quot;:&quot;Not authorized to access resource&quot; }'"/>
   	<xsl:call-template name="unauthorized"/>
   	</xsl:template>
</xsl:stylesheet>