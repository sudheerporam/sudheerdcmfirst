<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common" 
     xmlns:dp="http://www.datapower.com/extensions"
     xmlns:dpconfig="http://www.datapower.com/param/config" 
    exclude-result-prefixes="dp exsl"
    extension-element-prefixes="dp dpconfig">    
  <xsl:include href="local:/DiskStore/Common/XSLT/setHTTPStatusCode.xsl"/>	
  <xsl:import href="local:///DiskStore/Common/XSLT/utils.xsl"/>
    <xsl:template match="/">
        <xsl:message dp:priority="debug">**Executing authenticateRestConsumer.xsl**</xsl:message>
        
        	<xsl:variable name="AAAConfig" select="document('local:///DiskStore/APIGateway/config/AAAInfo.xml')"/>
       		<xsl:variable name="env" select="dp:variable('var://service/domain-name')"/>
       		<xsl:variable name="pingConfig" select="document('local:///DiskStore/APIGateway/config/AAAConfig.xml')/AAAConfig/env[@name=$env]"/>
			<xsl:variable name="pingURL" select="$pingConfig/pingDetails/URL/text()"/>
			<xsl:variable name="pingSSL" select="$pingConfig/pingDetails/proxyProfile/text()"/>
       		<xsl:variable name="pingBasicAuth" select="$pingConfig/pingDetails/basicAuth/text()"/>
        	 <xsl:message dp:priority="debug">AAAConfig:<xsl:copy-of select="$AAAConfig"/></xsl:message>
        	 <dp:set-variable name="'var://context/Gateway/AAAConfig'" value="$AAAConfig"/>
        	 <xsl:variable name="PASCheck" select="dp:http-request-header('application')"/>      
        	 <xsl:variable name="incomingUri" select="dp:variable('var://service/URI')"/> 	 
        	 <xsl:variable name="applicationInput">
        	 <!-- This check is for PAS Telematics API, as they cannot sent X-ApplicationContext -->
        	 <xsl:choose>
        	 	<xsl:when test="$PASCheck='PAS' and  contains($incomingUri,'/pas/v1/telematics/vehicles/')">
        	 		<xsl:value-of select="$PASCheck"/>
        	 	</xsl:when>
        	 	<xsl:otherwise>
        	  		<xsl:value-of select="/identity/entry/application/text()"/>
        	  	</xsl:otherwise>
        	  </xsl:choose>
        	  </xsl:variable>
        	 <xsl:variable name="oAuthFlag" select="/identity/entry/oAuthFlag/text()"/>
        	 <xsl:choose>
        	 	<xsl:when test="$oAuthFlag ='true'">
        	 		<xsl:variable name="authToken" select="/identity/entry/authToken/text()"/>
					<xsl:variable name="authHeader">
						<header name="Authorization"><xsl:value-of select="$pingBasicAuth"/></header>
					</xsl:variable>
					<xsl:variable name="beforePingCall" select="dp:variable('var://service/time-elapsed')"/>
					<xsl:variable name="pingCallResponse">
						<dp:url-open target="{$pingURL}" http-method="post" response ="responsecode-binary" ssl-proxy = "{$pingSSL}" content-type = "application/x-www-form-urlencoded" timeout = "4" http-headers="$authHeader">
							<xsl:value-of select="concat('grant_type=urn:pingidentity.com:oauth2:grant_type:validate_bearer&amp;token=',$authToken)"/>
						</dp:url-open>
					</xsl:variable>
        	 		<xsl:variable name="afterPingCall" select="dp:variable('var://service/time-elapsed')"/>
        	 		<xsl:call-template name="set-bam-config">
							<xsl:with-param name="key" select="'|authLatency='"/>
							<xsl:with-param name="value" select="concat(($afterPingCall -  $beforePingCall),'ms')"/>
					</xsl:call-template>
        	 		<xsl:variable name="httpRtnCd" select="$pingCallResponse/result/responsecode"/>
        	 		<xsl:choose>
        	 		<xsl:when test="string($httpRtnCd) !='200'">
        	 			<xsl:call-template name="setErrorObj"/>
        	 		</xsl:when>
        	 		<xsl:otherwise>
        	 			<xsl:variable name="OutputCred" select="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authenticate/Application[Name/text()=normalize-space($applicationInput)]/OutputCredential"/>
        	 			<authenticated><xsl:copy-of select="$OutputCred"/></authenticated>
        	 		</xsl:otherwise>
        	 		</xsl:choose>
        	 		
        	 		
        	 	</xsl:when>
        	 	<xsl:otherwise>
					
        	 		<xsl:variable name="clientId" select="/identity/entry/clientId/text()"/>
        	 		<xsl:choose>
        	 			<xsl:when test="$AAAConfig">      	 
        	 				<xsl:variable name="OutputCred" select="$AAAConfig/AAAConfig/Domain[@name=$env]/AAAConfig/Authenticate/Application[Name/text()=normalize-space($applicationInput) and ClientId/text()=normalize-space($clientId)]/OutputCredential"/>
        	 				<xsl:choose>
        	 				
        	 				<xsl:when test="string-length($OutputCred) &lt; 1">
                        	<xsl:message dp:type="custom-aaa-audit" dp:priority="info"><xsl:value-of select="'Transaction Rejected as the ApplicationContext was not populated or Invalid !!'"/></xsl:message>
                        	<!-- <dp:reject/> -->
                        	<xsl:call-template name="setErrorObj"/>
        	 				</xsl:when>
        	 				<xsl:otherwise>
        	 					<authenticated><xsl:copy-of select="$OutputCred"/></authenticated>
        	 				</xsl:otherwise>
        	 				</xsl:choose>
        	 				        	 				
        	 			</xsl:when>
        	 			<xsl:otherwise>
        	 			<!--  AU Fails -->
        	 				<xsl:message dp:type="custom-aaa-audit" dp:priority="info"><xsl:value-of select="'Transaction Rejected due to failure in reading AAAInfo.xml'"/></xsl:message>
        	 				<xsl:call-template name="setErrorObj"/>
        	 			</xsl:otherwise>
        	 		</xsl:choose>        	 	
        	 	</xsl:otherwise>
        	 </xsl:choose>
        	 
        	 
        	 

    </xsl:template>
    <xsl:template name="setErrorObj">
    <dp:set-variable name="'var://context/error/errorObjS'" value="'{ &quot;httpErrCd&quot;:&quot;401&quot;, &quot;errorSource&quot;:&quot;AAA authenciation xslt&quot;, &quot;httpErrMsg&quot;:&quot;Unauthorized&quot;, &quot;moreInfo&quot;:&quot;Mandatory credentials missing or incorrect&quot; }'"/>
   	<xsl:call-template name="unauthorized"/>
    </xsl:template>
    
</xsl:stylesheet>